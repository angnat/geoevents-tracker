var app = angular.module('geoevents', ['ionic', 'ngCordova']);

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});


app.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider

  .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  .state('tab.gps', {
    url: '/gps',
    views: {
     'tab-gps': {
       templateUrl: 'templates/tab-gps.html',
     }
    }
  })

  .state('tab.qrscan', {
    url: '/qrscan',
    views: {
      'tab-qrscan': {
        templateUrl: 'templates/tab-qrscan.html'
      }
    }
  })

  $urlRouterProvider.otherwise('/tab/gps');
});//config


app.controller('gpsCtrl', function($scope, $cordovaGeolocation, $timeout, $ionicPopup){

  var _ln = '\n';
  var posOptions = {
            enableHighAccuracy: true,
            timeout: 20000,
            maximumAge: 0
        };
  var DST_LAT = null;
  var DST_LNG = null;
  var SRC_LAT = null;
  var SRC_LNG = null;

  $scope.getGpsLocation = function(){
    $scope.TEST_LOG = 'retrieving your gps coordinates...'+_ln;

    $cordovaGeolocation.getCurrentPosition(posOptions).then(
      function (position)
      {
        var lat  = position.coords.latitude;
        var long = position.coords.longitude;
        var accuracy = position.coords.accuracy;
        var timestamp = position.timestamp;
        var altitude = position.coords.altitude;
        var altitudeAccuracy = position.coords.altitudeAccuracy;
        var heading = position.coords.heading;
        var speed = position.coords.speed;

        SRC_LAT = lat;
        SRC_LNG = long;

        var myLatlng = new google.maps.LatLng(lat, long);

        var mapOptions = {
            center: myLatlng,
            zoom: 17,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var marker = new google.maps.Marker({
            position: myLatlng,
            title:"You're here!"
        });

        var _map = new google.maps.Map(document.getElementById("map"), mapOptions);
        marker.setMap(_map);

        $timeout(function(){
          $scope.TEST_LOG += 'Got GPS Coordinates!' + _ln;
          $scope.TEST_LOG += 'Latitude: '           + lat       + _ln +
                             'Longitude: '          + long      + _ln +
                             'Accuracy: '           + accuracy  + _ln +
                             'Timestamp: '          + timestamp;

          $scope.map = _map;
        });//apply
      },
      function(err)
      {
        $timeout(function(){
          $scope.TEST_LOG += err;
        });//apply
      });
  }; //Fn getGpsLocation

  $scope.setGpsLocation = function(){
    var _dst_lat = null;
    var _dst_lng = null;

    $ionicPopup.prompt({
       title: 'Destination Coordinates',
       template: 'Enter Destination Coordinates',
       inputType: 'text',
       inputPlaceholder: 'Latitude, Longitude',
     }).then(function(res) {
       var _arr = res.split(',');
       _dst_lat = parseFloat(_arr[0]);
       _dst_lng = parseFloat(_arr[1]);

       DST_LAT = _dst_lat;
       DST_LNG = _dst_lng;

       $timeout(function(){
         $scope.TEST_LOG += '\nDESTINATION COORDINATES RECEIVED!\n';
         $scope.TEST_LOG += 'Destination Longitude: ' + DST_LNG + '\n';
         $scope.TEST_LOG += 'Destination Latitude: ' + DST_LAT + '\n';
       });//timeout
     });
  }; //Fn setGpsLocation

  $scope.plotRoute = function(){

    if(DST_LAT == null || DST_LNG == null || SRC_LAT == null || SRC_LNG == null){
      $ionicPopup.alert({
       title: 'Plot Error',
       template: '<p>You must get your current location, and also set your destination coordinates'+
                 ' before trying to plot your route!</p>'
     });
     return;
    }

    $timeout(function(){
      $scope.TEST_LOG += 'PLOTTING ROUTE...\n';
      $scope.TEST_LOG += 'Start Coordinates (Lat, Lng): ' + SRC_LAT + ', ' + SRC_LNG + '\n';
      $scope.TEST_LOG += 'End Coordinates (Lat, Lng): ' + DST_LAT + ', ' + DST_LNG + '\n';
    });

    var _start_pt = new google.maps.LatLng(SRC_LAT,SRC_LNG);
    var _end_pt = new google.maps.LatLng(DST_LAT,DST_LNG);
    var _wps = [{ location: _start_pt }, { location: _end_pt }];

    var mapOptions = {
        zoom: 9,
        center: _start_pt,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var _map = new google.maps.Map(document.getElementById("map"), mapOptions);
    var _directionsService = new google.maps.DirectionsService;
    var _directionsDisplay = new google.maps.DirectionsRenderer;
    _directionsDisplay.setMap(_map);

    _directionsService.route({
      origin: _start_pt,
      destination: _end_pt,
      waypoints: _wps,
      optimizeWaypoints: true,
      travelMode: google.maps.TravelMode.DRIVING
    }, function(response, status) {
      if (status === google.maps.DirectionsStatus.OK) {
        //success
        _directionsDisplay.setDirections(response);

        var route = response.routes[0];

        $timeout(function(){
          $scope.TEST_LOG += '\nROUTE PLOT SUCCESSFUL!';
          for (var i = 0; i < route.legs.length; i++) {
            var routeSegment = i + 1;
            $scope.TEST_LOG += '\n*** Route Segment ' + routeSegment +
                ' ***\n';
            $scope.TEST_LOG += '(' + route.legs[i].start_address + ') TO ';
            $scope.TEST_LOG += '(' + route.legs[i].end_address + ') \n';
            $scope.TEST_LOG += 'Avg. Distance: ' + route.legs[i].distance.text + '\n';
          }
        });//timeout


      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });


  }; //Fn plotRoute

});//gps controller


app.controller('qrCtrl', function($scope, $cordovaBarcodeScanner, $timeout){

  $scope.scanCode = function(){

    var _ln = '\n';
    $scope.TEST_LOG = "Initializing scanner..." + _ln;

    $cordovaBarcodeScanner.scan().then(
      function(imageData) {
        var text = imageData.text;
        var codeFormat = imageData.format;
        var isCancelled = (imageData.cancelled) ? 'Yes' : 'No';

        $timeout(function(){
          $scope.TEST_LOG += "Code retreived!" + _ln;
          $scope.TEST_LOG += 'Code Text: ' + text + _ln;
          $scope.TEST_LOG += 'Code Format: ' + codeFormat + _ln;
          $scope.TEST_LOG += 'Did user cancel scan? ' + isCancelled;
        });//timeout

       }, function(error) {
            $timeout(function(){
              $scope.TEST_LOG += 'An error occured: ' + error;
            }); //timeout
       });
  }; //Fn scanCode

});//QR controller
