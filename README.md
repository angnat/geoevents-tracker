# README #

GeoEvents Tracker

### What is this repository for? ###

This application is a cross-platform mobile app developed using Cordova/Phonegap.   
It scans a QR Code which returns an API link with info for a specific event.   
The GPS coordinates of the event is retrieved and mapped against your current location to show how far/close to the event you are.

### Author ###

* AngNat Technologies